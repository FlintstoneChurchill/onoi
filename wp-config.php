<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'onoi');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[H g$>H-@G{9-wB0b^c`9t!g,U&`Q&I_P^{$sLXd?,Qcxv^1tH3vr>EqL!a.&tPN');
define('SECURE_AUTH_KEY',  'xF:xWH@T92CzN70N1 []#w2[6^QaZO<S/e9< Z<`Dx#/I.W:YB3Y3Fdc5^;{3~;[');
define('LOGGED_IN_KEY',    'FGI=`@d~{5c K)MPUA)1yU^oa3&%{ip`q] ]xhjK.~!;D@z^p{v^~3-*6~nU(f>e');
define('NONCE_KEY',        '-f@N7_m*!q!UK^BLAFOAj-dBMMCc2j@}&Al=0}W6}>TmMR1dH.UP-tq0k6MOYD0^');
define('AUTH_SALT',        'z/q5Dgy](-ScU8,W)Ccs`m&fMFW2%<P`RW8l} 4I|zB?m>a_hl^(<MfU,&U1wzeb');
define('SECURE_AUTH_SALT', 'DW,KaE[I-]Oo^a.?kSovYxhp/w#8_Xj3+>{cF2~*o+%RIFLa?0(tRL@4_j`~%A$t');
define('LOGGED_IN_SALT',   '~| vT?NB^#rq@=qqacik%K{j Qp}8vAx`4}VZ6xq^0A&1F<{O8o*=7<JJlcS2c/ ');
define('NONCE_SALT',       'P&A;9xq03s=IFo,A:rLAE:zZHLo5;2h.s_#>RJMu-8nfnR^c^ODo?%d/*/p;Qt-G');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('FS_METHOD', 'direct');
define('WP_MEMORY_LIMIT', '64M');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

