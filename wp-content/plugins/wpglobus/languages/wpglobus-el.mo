��          �      l      �     �     �     �     	          .     3  #   D     h  !   �     �     �  	   �     �     �     �     �  $   �     $  &   :  �  a     2     ;  ;   W  #   �  2   �     �  %   �  @     3   `  H   �  (   �               4     =      ]     ~  $   �     �     �                                            	                     
                                 &hellip; Custom Code Draft saved at %s. Enter title here Error while saving. Help Invalid taxonomy Last edited by %1$s on %2$s at %3$s Last edited on %1$s at %2$s No items of this type were found. Select an item Settings Uninstall WPGlobus Word count: %s You are customizing %s g:i:s a https://github.com/WPGlobus/WPGlobus https://wpglobus.com/ word count: words or characters?words Project-Id-Version: WPGlobus Multilingual
Report-Msgid-Bugs-To: support@wpglobus.com
POT-Creation-Date: 2018-03-23 08:30-0400
PO-Revision-Date: 2018-03-16 21:53+0000
Last-Translator: Gregory Karpinsky <gregory@tiv.net>
Language-Team: Greek (http://www.transifex.com/wp-translations/wpglobus-multilingual/language/el/)
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 &hellip; Κωδικός Πελάτη Το πρόχειρο αποθηκεύτηκε στις %s. Δώστε τον τίτλο εδώ Σφάλμα κατά την αποθήκευση! Βοήθεια Εσφαλμένη ταξινομία Τελευταία επεξεργασία από %1$s, %2$s %3$s Τελευταία επεξεργασία %1$s %2$s Βρέθηκε.χωρίς στοιχεία αυτού του τύπου. Επιλέξτε ένα στοιχείο Ρυθμίσεις Απεγκατάσταση WPGlobus Αριθμός λέξεων: %s Προσαρμόζετε το %s H:i:s https://github.com/WPGlobus/WPGlobus https://wpglobus.com/ λέξεις 