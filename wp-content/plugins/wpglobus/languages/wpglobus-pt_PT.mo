��    %      D  5   l      @     A     J  
   R  
   ]     h     t     |     �     �     �     �     �     �     �     �  	   �     �     �  #   �     !     =  !   B     d     h  
   k  
   v     �     �     �     �     �     �  	   �     �     �  &   �  �  �     �     �     �               *     9     B     N     f     m     t     �     �     �  	   �     �     �  ,   �  "        4  &   :     a     f     j     �     �     �  
   �     �     �     �     �     �     �     �     $              "                       %                    !                                 #   
                                                         	                        &hellip; Add-ons Contact Us Custom CSS Custom Code Default Delete Description Draft saved at %s. Edit Enable Enter title here Error while saving. FAQ Help Installed Invalid taxonomy Label Last edited by %1$s on %2$s at %3$s Last edited on %1$s at %2$s List No items of this type were found. OFF ON Post Types Post types Redirect Save Changes Select Settings Support Title Uninstall Word count: %s g:i:s a word count: words or characters?words Project-Id-Version: WPGlobus Multilingual
Report-Msgid-Bugs-To: support@wpglobus.com
POT-Creation-Date: 2018-03-23 08:30-0400
PO-Revision-Date: 2018-03-16 21:53+0000
Last-Translator: Gregory Karpinsky <gregory@tiv.net>
Language-Team: Portuguese (Portugal) (http://www.transifex.com/wp-translations/wpglobus-multilingual/language/pt_PT/)
Language: pt_PT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 &hellip; Add-ons Contacte-nos CSS personalizado Código personalizado Predefinição Eliminar Descrição Rascunho guardado em %s Editar Ativar Introduza o título aqui Erro ao salvar. Perguntas Mais Frequentes Ajuda Instalado Taxonomia inválida Etiqueta Última edição por %1$s, em %2$s às %3$s  Última edição em %1$s às %2$s  Lista Nenhum item deste tipo foi encontrado. Não Sim Tipos de Publicação Tipos de artigo Redirecionar Guardar Alterações Selecionar Definições Apoio Título Desinstalar Palavras: %s G:i:s words 