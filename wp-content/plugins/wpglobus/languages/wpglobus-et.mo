��    U      �  q   l      0     1     5     B     P     a      i     �  
   �     �  
   �     �     �     �     �     �     �     �          	          1     B     F     K     [     `     e  
   k     v     �     �     �  	   �  	   �     �     �     �     �     �     	     	  	    	     *	     :	     A	     F	     V	     [	     k	  !   z	     �	     �	     �	     �	  *   �	     �	  !   
  
   >
     I
     R
     d
     q
     y
     �
     �
     �
     �
     �
     �
  #   �
     �
                         -     6     E     T  $   l     �     �     �  &   �  �  �     �  	   �  
   �     �     �  )   �          
     !     3     B     U     d     l  	   y     �     �     �     �     �     �     �     �     �     �     �     �                    &     -     5     B  	   N     X  	   ]     g       
   �  
   �     �     �     �     �     �     �     �     �  $        =     E     J     _  (   x     �     �     �     �     �     
  	        (  	   -  
   7     B     I     b     o     ~     �     �     �     �     �     �     �     �       $        >     T     Z     a     0   .   H          S   ,   =                
                      K   B   A      2   R           '   +          (   )   T         !          "           /          :      4   P   F   3   &   M   I           G      8   >         ?          L   *          O   %   9          C      7   E   U   -   6          1   ;       Q      @   N   $                 D   <          5                                   #   J       	    Add Add Language Add Languages Add new Language Add-ons Are you sure you want to delete? Code Contact Us Current Version Custom CSS Custom JS Code Default Delete Delete Language Description Edit Edit Language Enable Enabled Languages English language name Enter title here FAQ FAQs Fields Settings File Flag Flags Flags only Get it now! Get it now: Guide Help Help Desk Installed Instructions: Label Language Code Language Selector Mode Language code already exists! Language flag Language name Languages Languages table Links: List List with flags Name Name in English No items found No items of this type were found. OFF ON Options updated Please enter a language code! Please enter the language name in English! Please enter the language name! Please specify the language flag! Post types Redirect Save &amp; Reload Save Changes Section Select Select a language Select an item Settings Show all sections Support Technical Information The name of the language in English Title Turn off Turn on WPGlobus WPGlobus Plus! Welcome! Word count: %s YES, I CONFIRM You have been warned... https://github.com/WPGlobus/WPGlobus https://wpglobus.com/ item items word count: words or characters?words Project-Id-Version: WPGlobus Multilingual
Report-Msgid-Bugs-To: support@wpglobus.com
POT-Creation-Date: 2018-03-23 08:30-0400
PO-Revision-Date: 2018-03-16 21:53+0000
Last-Translator: Gregory Karpinsky <gregory@tiv.net>
Language-Team: Estonian (http://www.transifex.com/wp-translations/wpglobus-multilingual/language/et/)
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Lisa Lisa keel Lisa keeli Lisa uus keel Lisad Oled sa kindel, et soovid seda kustutada? Kood Võta meiega ühendust Praegune versioon Kohandatud CSS Kohandatud JS kood Vaikeväärtus Kustuta Kustuta keel Kirjeldus Muuda Muuda keelt Luba Lubatud keeled Keele nimi inglise keeles Sisesta pealkiri siia KKK KKK Väljade seaded Fail Lipp Lipid Ainult lipud Hangi kohe! Hangi kohe: Juhend Abiinfo Kasutajatugi Paigaldatud Juhendid: Silt Keelekood Keelte valimise režiim Keelekood on juba olemas! Keele lipp Keele nimi Keeled Keeltetabel Lingid: Nimekiri Nimekiri lippudega Nimi Nimi inglise keeles Ühtegi kirjet ei leitud Ühtegi seda liiki kirjet ei leitud. VÄLJAS SEES Valikud on uuendatud Palun sisesta keelekood! Palun sisesta keele nimi inglise keeles! Palun sisesta keele nimi! Palun määra keele lipp! Postituse liik Suuna ümber Salvesta ja laadi uuesti Salvesta muudatused Sektsioon Vali Vali keel Vali kirje Seaded Näita kõiki sektsioone Kasutajatugi Tehniline info Keele nimi inglise keeles Pealkiri Lülita välja Lülita sisse WPGlobus WPGlobus Plus! Teretulemast! Sõnade arv: %s JAH, OLEN NÕUS Sind on hoiatatud... https://github.com/WPGlobus/WPGlobus https://wpglobus.com/ kirje kirjet sõna 