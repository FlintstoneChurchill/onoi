<?php
//jquery to top
function mh_load_my_script() {
    wp_enqueue_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'mh_load_my_script' );


// General setup
function wtc_setup() {
	// Tell the TinyMCE editor to use a custom stylesheet
	add_editor_style('/css/editor-style.css');
	
	//Theme support
	add_theme_support('post-thumbnails');
	add_image_size( 'thumb', 600, 400, true );
	add_image_size( 'square_small', 200, 200, true );

	// Register nav menus
	register_nav_menus(
		array(
			'top_menu' => 'Top Menu'
		)
	);
}
add_action('after_setup_theme', 'wtc_setup');
 
if(is_admin_bar_showing()) {
    show_admin_bar(false);    
}


// function wtc_styles() {
	// wp_enqueue_style('font_roboto', 'http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic');
	// wp_enqueue_style('font_hightower', get_template_directory_uri() . '/fonts/hightower.css');
	// wp_enqueue_style('bootstrap_style', get_template_directory_uri() . '/css/bootstrap.min.css');
	// wp_enqueue_style('bootstrap_select_style', get_template_directory_uri() . '/css/bootstrap-select.min.css');
	// wp_enqueue_style('owl_style', get_template_directory_uri() . '/css/owl.carousel.css');
	// wp_enqueue_style('animate_style', get_template_directory_uri() . '/css/animate.css');
	// wp_enqueue_style('theme_style', get_template_directory_uri() . '/css/main.css');
	
// 	wp_register_script('bootstrap_script', get_template_directory_uri() . '/js/libs/bootstrap.min.js', array( 'jquery') );
// 	wp_enqueue_script('bootstrap_script');
// 	wp_register_script('bootstrap_select_script', get_template_directory_uri() . '/js/bootstrap-select.min.js', array( 'jquery') );
// 	wp_enqueue_script('bootstrap_select_script');
// 	wp_register_script('owl_script', get_template_directory_uri() . '/js/owl.carousel.min.js', array( 'jquery') );
// 	wp_enqueue_script('owl_script');
// 	wp_register_script('theme_script', get_template_directory_uri() . '/js/scripts.js', array( 'jquery') );
// 	wp_enqueue_script('theme_script');
// }
// add_action('wp_enqueue_scripts', 'wtc_styles', '999');

// function footer_enqueue_scripts(){
// 	remove_action('wp_head','wp_print_scripts');
// 	remove_action('wp_head','wp_print_head_scripts',9);
// 	remove_action('wp_head','wp_enqueue_scripts',1);
// 	add_action('wp_footer','wp_print_scripts',5);
// 	add_action('wp_footer','wp_enqueue_scripts',5);
// 	add_action('wp_footer','wp_print_head_scripts',5);
// }
// add_action('after_setup_theme','footer_enqueue_scripts');


//Replace default title
add_filter( 'wp_title', 'baw_hack_wp_title_for_home' );
function baw_hack_wp_title_for_home( $title ) {
	if( empty( $title ) && ( is_home() || is_front_page() ) ) {
		return $title . get_bloginfo( 'description' );
	}
	return $title;
}

// Admin load css
function wtc_load_admin_style() {
	wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/css/admin.css', false, '1.0.0' );
	wp_enqueue_style( 'custom_wp_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'wtc_load_admin_style' );


//Add Missing Alt Tags To WordPress Images
function add_alt_tags($content) {
	global $post;
	preg_match_all('/<img (.*?)\/>/', $content, $images);
	if(!is_null($images)) {
		foreach($images[1] as $index => $value) {
			if(!preg_match('/alt=/', $value))  {
				$new_img = str_replace('<img', '<img alt="'.$post->post_title.'"', $images[0][$index]);
				$content = str_replace($images[0][$index], $new_img, $content);
			}
		}
	}
	return $content;
}
add_filter('the_content', 'add_alt_tags', 99999);


//Embed Video Fix
function add_video_wmode_transparent( $html ) {
	$pattern = '#(src="https?://www.youtube(?:-nocookie)?.com/(?:v|embed)/([a-zA-Z0-9-]+).")#';
	preg_match_all( $pattern, $html, $matches );
	 
	if ( count( $matches ) > 0) {
		foreach ( $matches[0] as $orig_src ) {
			if ( !strstr($orig_src, 'wmode=transparent' )) {
				$add = 'wmode=transparent"';
				 
				if ( !strstr($orig_src, '?') ) {
					$add = '?' . $add;
				}
				$new_src = substr( $orig_src, 0, -1 ) . $add;
				$html = str_replace( $orig_src, $new_src, $html );
			}
		}
	}
	return $html;
}
add_filter('the_excerpt', 'add_video_wmode_transparent', 10);
add_filter('the_content', 'add_video_wmode_transparent', 10);

// Cut string with words
function cut_string($str, $length, $postfix='...', $encoding='UTF-8')
{
    if (mb_strlen($str, $encoding) <= $length) {
        return $str;
    }
 
    $tmp = mb_substr($str, 0, $length, $encoding);
    return mb_substr($tmp, 0, mb_strripos($tmp, ' ', 0, $encoding), $encoding) . $postfix;
}


// Sets the post excerpt length to selected words.
function wtc_excerpt_length( $length ) {
	return 15;
}
add_filter( 'excerpt_length', 'wtc_excerpt_length' );


// Remove MORE link from the post excerpt. Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis.
function wtc_remove_excerpt_more( $more ) {
	return '&hellip;';
}
add_filter( 'excerpt_more', 'wtc_remove_excerpt_more' );


//Disable Srcset
function disable_srcset( $sources ) {
	return false;
}
add_filter( 'wp_calculate_image_srcset', 'disable_srcset' );
//Remove the reponsive stuff from the content
remove_filter( 'the_content', 'wp_make_content_images_responsive' );
remove_filter( 'wp_head', 'wp_make_content_images_responsive' );

//Disable rss
function fb_disable_feed() {
	wp_die( __('No feed available,please visit our <a href="'. get_bloginfo('url') .'">homepage</a>!') );
}
add_action('do_feed', 'fb_disable_feed', 1);
add_action('do_feed_rdf', 'fb_disable_feed', 1);
add_action('do_feed_rss', 'fb_disable_feed', 1);
add_action('do_feed_rss2', 'fb_disable_feed', 1);
add_action('do_feed_atom', 'fb_disable_feed', 1);
add_action('do_feed_rss2_comments', 'fb_disable_feed', 1);
add_action('do_feed_atom_comments', 'fb_disable_feed', 1);

remove_filter('the_content', 'wpautop');

function remove_menus(){
  
  remove_menu_page( 'index.php' );                  //Dashboard
  remove_menu_page( 'jetpack' );                    //Jetpack* 
  remove_menu_page( 'edit.php' );                   //Posts
  remove_menu_page( 'upload.php' );                 //Media
  remove_menu_page( 'edit.php?post_type=page' );    //Pages
  remove_menu_page( 'edit-comments.php' );          //Comments
  remove_menu_page( 'themes.php' );                 //Appearance
  remove_menu_page( 'plugins.php' );                //Plugins
  remove_menu_page( 'users.php' );                  //Users
  remove_menu_page( 'tools.php' );                  //Tools
  remove_menu_page( 'options-general.php' );        //Settings
  
}
add_action( 'admin_menu', 'remove_menus' );
