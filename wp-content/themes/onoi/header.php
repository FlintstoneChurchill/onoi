<!DOCTYPE html>
<!--[if IE 7]><html <?php language_attributes(); ?> class="ie7"><![endif]-->
<!--[if IE 8]><html <?php language_attributes(); ?> class="ie8"><![endif]-->
<!--[if IE 9]><html <?php language_attributes(); ?> class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><html <?php language_attributes(); ?>><![endif]-->
<!--[if !IE]><html <?php language_attributes(); ?>><![endif]-->
<html <?php language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php wp_title('|', true, 'right'); ?></title>

     <?php wp_head(); ?>

    <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/fontawesome-all.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/styles.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/media.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,700&amp;subset=cyrillic" rel="stylesheet">
    <script src="<?php bloginfo('template_url') ?>/js/jquery.min.js"></script>
    <script src="<?php bloginfo('template_url') ?>/js/owl.carousel.min.js"></script>
    <script  src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.18/jquery.touchSwipe.min.js"></script>
    <script src="<?php bloginfo('template_url') ?>/js/core.js"></script>
</head>

<body <?php body_class(); ?>>
    <div class="header">
        <div class="container">
            <?php if (has_nav_menu('top_menu')) :
                wp_nav_menu(array('theme_location' => 'top_menu'));
            endif; ?>
            <div class="header_social">
                <a href="#">
                    <i class="fab fa-facebook-f"></i>
                </a>
                <a href="#">
                    <i class="fab fa-instagram"></i>
                </a>
                <a href="#">
                    <i class="fab fa-twitter"></i>
                </a>
            </div>
        </div>
    </div>