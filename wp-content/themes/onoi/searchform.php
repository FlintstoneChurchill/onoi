<form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
    <input type="search" placeholder="Search" value="<?php echo get_search_query() ?>" name="s" class="header_search_field" />
    <button type="submit"><span class="glyphicon glyphicon-search"></span></button>
</form>