$(function() {
  $(".advantages_slider").owlCarousel({
    loop: true,
    items: 1,
    nav: false,
    dots: true
  });

  $(".info_slider div[data-slider_item]").on("click", function() {
    $(this)
      .parent()
      .find(".prime")
      .removeClass("prime")
      .addClass($(this).attr("class"));
    $(this)
      .removeClass($(this).attr("class"))
      .addClass("prime");
  });

  var rotate, timeline;

  rotate = function() {
    return $(".card:first-child")
      .fadeOut(400, "swing", function() {
        return $(".card:first-child")
          .appendTo(".info_slider_mobile")
          .hide();
      })
      .fadeIn(400, "swing");
  };

//   timeline = setInterval(rotate, 1200);

//   $("body").hover(function() {
//     return clearInterval(timeline);
//   });

  $(".card").click(function() {
    return rotate();
  });

//   $(".advantages").mousemove(function(e) {
//     /* Work out mouse position */
//     var offset = $(this).offset();
//     var xPos = e.pageX - offset.left;
//     var yPos = e.pageY - offset.top;

//     var mouseXPercent = Math.round(xPos / $(this).width() * 100);
//     var mouseYPercent = Math.round(yPos / $(this).height() * 100);

//     $(this)
//       .find(".parallax")
//       .each(function() {
//         var diffX = $(".advantages").width() - $(this).width();
//         var diffY = $(".advantages").height() - $(this).height();

//         var myX = diffX * (mouseXPercent / -2000);
//         var myY = diffY * (mouseYPercent / -2000);
//         var cssObj = { left: myX + "px", top: myY + "px" };
//         $(this).animate(
//           { left: myX, top: myY },
//           { duration: 50, queue: false }
//         );
//       });
//   });

  if ($(window).width() > 768) {
    $(".main_block .main_block_column.left, .main_block .main_block_column.right").addClass("visible");
    $(window).on("scroll", function() {
      if ($(window).scrollTop() >= $(".our_advantages").offset().top) {
        $(".advantages_item").addClass("visible");
      }
    });
  } 
  
  if ($(window).width() <= 400) {
    console.log('less then 400')
    $(".swipe").swipe( {
      //Generic swipe handler for all directions
      swipe:function(event, direction) {
        if (direction === 'up') {
          $("html, body").animate({scrollTop: $(this).next().offset().top}, 500) 
        } 
        if (direction === 'down') {
          $("html, body").animate({scrollTop: $(this).prev().offset().top}, 500)
        }
      }
    });
  }
  
});

$(window).on('resize', function() {
    if ($(window).width() <= 400) {
        console.log('less then 400')
        $(".swipe").swipe( {
          //Generic swipe handler for all directions
          swipe:function(event, direction) {
            if (direction === 'up') {
              $("html, body").animate({scrollTop: $(this).next().offset().top}, 500) 
            } 
            if (direction === 'down') {
              $("html, body").animate({scrollTop: $(this).prev().offset().top}, 500)
            }
          }
        });
  } else {
    $(".swipe").swipe("destroy");
    $(".our_advantages").swipe("destroy");
    $(".info_block").swipe("destroy");
    $(".advantages").swipe("destroy");
    $(".download").swipe("destroy");
  }
})

