<?php get_header(); ?>
	
	<?php if ( have_posts() ) : ?>
		
		
		
		<div class="container content_container">
			<h1 class="page_title"><span>
				<?php
					foreach((get_the_category()) as $category) {
						echo $category->cat_name . ' ';
					}
				?>
			</span></h1>
			<h2 class="page_title blog_post_title"><?php the_title(); ?></h2>
			
			<div class="row">
				<div class="col-md-8">
					<?php while ( have_posts() ) : the_post(); ?>
						
						<?php $blog_media_type = types_render_field("blog-media-type", array("output"=>"raw")); ?>
							<?php if($blog_media_type == 1) { //featured ?>
								<div class="blog_media_single">
									<?php if ( has_post_thumbnail() ) { ?>
										<?php the_post_thumbnail('thumb', array( 'alt' => trim( strip_tags( get_the_title() ) ), ) ); ?>
									<?php } else { ?>
										<img src="<?php bloginfo('template_url'); ?>/images/noimage.jpg" alt="No Image" />
									<?php } ?>
								</div>
							<?php } elseif($blog_media_type == 2) { //gallery ?>
								<div class="blog_media_slider owl-carousel">
									<?php
										$blog_gallery = types_render_field("blog-gallery", array("url"=>"true", "size"=>"thumb"));
										$arr = explode( ' ', $blog_gallery );
										foreach ($arr as $value) {
									?>
											<div class="item"><img src="<?php echo $value; ?>" alt="<?php the_title(); ?>" /></div>
									<?php
										}
									?>
								</div>
								<script>
									jQuery(document).ready(function(){
										jQuery('.blog_media_slider').owlCarousel({
											loop:true,
											items: 1,
											nav: true,
											navText: ['<span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>','<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>']
										});
									});
								</script>
							<?php } elseif($blog_media_type == 3) { //video ?>
								<div class="blog_media_video embed-responsive embed-responsive-16by9">
									<?php echo types_render_field("blog-video", array("output"=>"raw")); ?>
								</div>
							<?php } ?>
							
						<div class="blog_post_wrap">
							
							<div class="blog_item_info">
								<span class="blog_item_info_item"><?php echo get_the_date("F j, Y"); ?></span>
								<a href="#respond" class="blog_item_info_item"><?php comments_number(); ?></a>
								<span class="blog_item_info_item blog_item_info_item_cats">Categories: <?php the_category(', '); ?></span>
							</div>
							
							<div class="blog_social">
								<div class="blog_social_item">
									<div id="fb-root"></div>
									<script>(function(d, s, id) {
									  var js, fjs = d.getElementsByTagName(s)[0];
									  if (d.getElementById(id)) return;
									  js = d.createElement(s); js.id = id;
									  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
									  fjs.parentNode.insertBefore(js, fjs);
									}(document, 'script', 'facebook-jssdk'));</script>
									<div class="fb-like" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
								</div>
								<div class="blog_social_item">
									<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
									<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
								</div>
								<div class="blog_social_item">
									<a href="//www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark" data-pin-zero="true" data-pin-config="beside"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" alt="Pin It" /></a>
									<script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
								</div>
							</div>
							
							<div class="page_content blog_page_content">
								<?php the_content(); ?>
							</div>
							
							<?php $post_tags = wp_get_post_tags($post->ID); ?>
							<?php if($post_tags) { ?>
								<!--<div class="sidebar_tags_wrap">
									<?php foreach($post_tags as $tag) { ?>
										<a href="<?php echo $tag->slug; ?>"><?php echo $tag->name; ?></a>
									<?php } ?>
								</div>-->
							<?php } ?>
						</div>
						
						<?php 
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						?>
						
					<?php endwhile; ?>
				</div>
				<div class="blog_sidebar col-md-4">
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>
		
	<?php else : ?>
		<?php get_template_part( 'content', '404' ); ?>
	<?php endif; // end have_posts() check ?>

<?php get_footer(); ?>