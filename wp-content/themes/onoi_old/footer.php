<div class="footer">
        <div class="footer_top">
            <div class="container">
                <div class="logo">
                    <img src="<?php bloginfo('template_url'); ?>/img/logo_white.png" alt="Оной">
                </div>
                <div class="footer_social">
                    <a href="#">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <a href="#">
                        <i class="fab fa-instagram"></i>
                    </a>
                    <a href="#">
                        <i class="fab fa-twitter"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="footer_bottom">
            <div class="container">
                Бишкек 2018
            </div>
        </div>
	</div>
	<?php wp_footer(); ?>
</body>

</html>