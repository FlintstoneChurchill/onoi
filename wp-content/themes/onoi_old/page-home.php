<?php
/**
 * Template Name: Homepage Template
 */
?>

<?php get_header(); ?>
<?php
    $loop = new WP_Query(
        array(
            'post_status' => 'publish',
            'post_type' => 'main_section',
            'posts_per_page' => -1,
        ) 
    );
?>
<?php if ( $loop->have_posts() ) { ?>
    <div class="main_block swipe">
        <div class="container">
            <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <div class="main_block_column left">
                    <div class="logo">
                        <img src="<?php bloginfo('template_url') ?>/img/logo.png" alt="Оной">
                    </div>
                    <p><?php the_content(); ?></p>
                    <h1><?php the_title(); ?></h1>
                    <span><?php the_field('download_info'); ?></span>
                    <div class="apps_block">
                        <a href="#" class="app_store"></a>
                        <a href="#" class="play_market"></a>
                    </div>
                </div>
                <div class="main_block_column right">
                    <?php if ( has_post_thumbnail() ) { ?>
                        <?php the_post_thumbnail('full', array( 'alt' => trim( strip_tags( get_the_title() ) ), ) ); ?>
                    <?php } ?>
                </div>
            <?php endwhile; ?>
            <a href="#" class="anchor"></a>
        </div>
    </div>
<?php } ?>
<?php
    $adv_loop = new WP_Query(
        array(
            'post_status' => 'publish',
            'post_type' => 'our_advantages',
            'posts_per_page' => -1,
        ) 
    );
?>
<?php if ( $adv_loop->have_posts() ) { ?>
    <div class="our_advantages swipe">
        <div class="container">
            <h2 class="title"><?php echo __('[:ru]Наши Преимущества[:kg]Биздин Артыкчылыктары[:]') ?></h2>
            <?php while ( $adv_loop->have_posts() ) : $adv_loop->the_post(); ?>
                <div class="advantages_item">
                    <h4 style="background-image: url('<?php echo get_field('title_icon')[url]; ?>')"><?php the_title(); ?></h4>
                    <p><?php the_content(); ?></p>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
<?php } ?>

<?php
    $pop_loop = new WP_Query(
        array(
            'post_status' => 'publish',
            'post_type' => 'popups',
            'posts_per_page' => -1,
        ) 
    );
?>

<?php if ( $pop_loop->have_posts() ) { ?>
    <div class="info_block swipe">
        <div class="container">
            <div class="info_block_title">
                <div class="info_logo">
                    <img src="<?php bloginfo('template_url'); ?>/img/logo.png" alt="Оной">
                </div>
            </div>
            <?php while ( $pop_loop->have_posts() ) : $pop_loop->the_post(); ?>
                <p class="info_title"><?php the_title(); ?></p>
                <div class="info_slider">
                    <?php 
                        $imageFields = get_fields(30);
                        $c = 0;
                        foreach ($imageFields as $key=>$value) { ?>
                            <div class="<?php echo $c === 0 ? 'prime' : 'left'.$c ?>" data-slider_item>
                                <img src="<?php echo $value[url]; ?>" alt="<?php $key ?>">
                            </div>

                            <?php $c++; ?>
                    <?php } ?>
                </div>
                <div class="info_slider_mobile">
                    <?php 
                        $c = 0;
                        foreach ($imageFields as $key=>$value) { ?>
                            <div class="card" data-card="<?php echo $c ?>">
                                <img src="<?php echo $value[url]; ?>" alt="<?php $key ?>">
                            </div>
                            <?php $c++; ?>
                    <?php } ?>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
<?php } ?>

<?php
    $slide_loop = new WP_Query(
        array(
            'post_status' => 'publish',
            'post_type' => 'slider',
            'posts_per_page' => -1,
        ) 
    );
?>
<?php if ( $slide_loop->have_posts() ) { ?>
    <div class="advantages swipe">
        <div class="container">
            <h2 class="title">
                <?php echo __('[:ru]Преимущества[:kg]Артыкчылыктары[:]') ?>
                <p class="subtitle">
                    <?php echo __('[:ru]Ознакомтесь с нашими преимуществами[:kg]Биздин артыкчылыктар менен таанышуу[:]') ?>
                </p>
            </h2>
            <div class="parallax">
                <div class="parallax_item item1"></div>
                <div class="parallax_item item2"></div>
                <div class="parallax_item item3"></div>
                <div class="parallax_item item4"></div>
                <div class="parallax_item item5"></div>
                <div class="parallax_item item6"></div>
                <div class="parallax_item item7"></div>
                <div class="parallax_item item8"></div>
                <div class="parallax_item item9"></div>
            </div>
            <div class="advantages_slider">
                <?php while ( $slide_loop->have_posts() ) : $slide_loop->the_post(); ?>
                    <div class="item">
                        <div class="left_column">
                            <?php if ( has_post_thumbnail() ) { ?>
                                <?php the_post_thumbnail('full', array( 'alt' => trim( strip_tags( get_the_title() ) ), ) ); ?>
                            <?php } ?>
                        </div>
                        <div class="right_column">
                            <h3><?php the_title(); ?></h3>
                            <p><?php the_content(); ?></p>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
<?php } ?>

<?php
    $app_loop = new WP_Query(
        array(
            'post_status' => 'publish',
            'post_type' => 'app',
            'posts_per_page' => -1,
        ) 
    );
?>

<?php if ( $app_loop->have_posts() ) { ?>

    <div class="download swipe">
        <div class="container">
            <?php while ( $app_loop->have_posts() ) : $app_loop->the_post(); ?>
                <div class="download_column left">
                    <h3><?php the_title(); ?></h3>
                    <p><?php the_content(); ?></p>
                    <div class="apps_block">
                        <a href="#" class="app_store"></a>
                        <a href="#" class="play_market"></a>
                    </div>
                </div>
                <div class="download_column right">
                    <?php if ( has_post_thumbnail() ) { ?>
                        <?php the_post_thumbnail('full', array( 'alt' => trim( strip_tags( get_the_title() ) ), ) ); ?>
                    <?php } ?>
                </div>
            <?php endwhile; ?>
        </div>
    </div>

<?php } ?>

<?php get_footer(); ?>
