<?php get_header(); ?>
	
	<?php //Remove Sticky post from posts list
		global $query_string;
		parse_str( $query_string, $args );
		$args['post__not_in'] = get_option('sticky_posts');
		query_posts($args);
	?>
	
	<?php if ( have_posts() ) : ?>
		
		<div class="container content_container">
			<h1 class="page_title"><span>
				<?php
					$category_id = get_cat_ID(single_cat_title( '', false ));
					$category = get_category($category_id);
					if ($category->category_parent > 0){
						$category_parent = get_category($category->category_parent);
						echo $category_parent->name;
					} else {
						echo single_cat_title( '', false );
					}
				?>
			</span></h1>
			<?php if ($category->category_parent > 0){ ?>
				<h2 class="page_title blog_post_title"><?php echo single_cat_title( '', false ); ?></h2>
			<?php } ?>
			
			<div class="row">
				<div class="col-md-8">
					
					<?php if(category_description()) { ?>
						<div class="blog_item blog_item_preview page_content" style="padding-top: 30px; padding-bottom: 30px;">
							<?php echo category_description(); ?>
						</div>
					<?php } ?>
					
					<?php /*
						if(!$paged) {
							$category = get_the_category();
							$loop = new WP_Query( //First select only main event
								array(
									'post_status' => 'publish',
									'post_type' => 'post',
									'cat' => $category_id,
									'post__in'  => get_option( 'sticky_posts' )
								) 
							);
							if ( $loop->have_posts() ) {
							while ( $loop->have_posts() ) : $loop->the_post();
								get_template_part( 'content', 'blog' );
							endwhile;
							wp_reset_postdata();
							}
						}
					*/?>
					
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'content', 'blog' ); ?>
					<?php endwhile; ?>
					<?php wtc_paging_nav(); ?>
				</div>
				<div class="blog_sidebar col-md-4">
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>
		
		<script>
			jQuery(document).ready(function(){
				jQuery('.blog_media_slider').owlCarousel({
					loop:true,
					items: 1,
					nav: true,
					navText: ['<span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>','<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>']
				});
			});
		</script>
		
	<?php else : ?>
		<?php get_template_part( 'content', '404' ); ?>
	<?php endif; // end have_posts() check ?>

<?php get_footer(); ?>
