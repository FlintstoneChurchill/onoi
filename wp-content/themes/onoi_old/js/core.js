(function () {
  // initializes touch and scroll events
      var supportTouch = $.support.touch,
          scrollEvent = "touchmove scroll",
          touchStartEvent = supportTouch ? "touchstart" : "mousedown",
          touchStopEvent = supportTouch ? "touchend" : "mouseup",
          touchMoveEvent = supportTouch ? "touchmove" : "mousemove";
   
      // handles swipe up and swipe down
      $.event.special.swipeupdown = {
          setup: function () {
              var thisObject = this;
              var $this = $(thisObject);
   
              $this.bind(touchStartEvent, function (event) {
                  var data = event.originalEvent.touches ?
                          event.originalEvent.touches[ 0 ] :
                          event,
                      start = {
                          time: (new Date).getTime(),
                          coords: [ data.pageX, data.pageY ],
                          origin: $(event.target)
                      },
                      stop;
   
                  function moveHandler(event) {
                      if (!start) {
                          return;
                      }
   
                      var data = event.originalEvent.touches ?
                          event.originalEvent.touches[ 0 ] :
                          event;
                      stop = {
                          time: (new Date).getTime(),
                          coords: [ data.pageX, data.pageY ]
                      };
   
                      // prevent scrolling
                      if (Math.abs(start.coords[1] - stop.coords[1]) > 10) {
                          event.preventDefault();
                      }
                  }
   
                  $this
                      .bind(touchMoveEvent, moveHandler)
                      .one(touchStopEvent, function (event) {
                          $this.unbind(touchMoveEvent, moveHandler);
                          if (start && stop) {
                              if (stop.time - start.time < 1000 &&
                                  Math.abs(start.coords[1] - stop.coords[1]) > 30 &&
                                  Math.abs(start.coords[0] - stop.coords[0]) < 75) {
                                  start.origin
                                      .trigger("swipeupdown")
                                      .trigger(start.coords[1] > stop.coords[1] ? "swipeup" : "swipedown");
                              }
                          }
                          start = stop = undefined;
                      });
              });
          }
      };
   
  //Adds the events to the jQuery events special collection
      $.each({
          swipedown: "swipeupdown",
          swipeup: "swipeupdown"
      }, function (event, sourceEvent) {
          $.event.special[event] = {
              setup: function () {
                  $(this).bind(sourceEvent, $.noop);
              }
          };
      });
   
  })();


$(function() {
  $(".advantages_slider").owlCarousel({
    loop: true,
    items: 1,
    nav: false,
    dots: true
  });

  $(".info_slider div[data-slider_item]").on("click", function() {
    $(this)
      .parent()
      .find(".prime")
      .removeClass("prime")
      .addClass($(this).attr("class"));
    $(this)
      .removeClass($(this).attr("class"))
      .addClass("prime");
  });

  var rotate, timeline;

  rotate = function() {
    return $(".card:first-child")
      .fadeOut(400, "swing", function() {
        return $(".card:first-child")
          .appendTo(".info_slider_mobile")
          .hide();
      })
      .fadeIn(400, "swing");
  };

  timeline = setInterval(rotate, 1200);

  $("body").hover(function() {
    return clearInterval(timeline);
  });

  $(".card").click(function() {
    return rotate();
  });

  $(".advantages").mousemove(function(e) {
    /* Work out mouse position */
    var offset = $(this).offset();
    var xPos = e.pageX - offset.left;
    var yPos = e.pageY - offset.top;

    /* Get per­cent­age positions */
    var mouseXPercent = Math.round(xPos / $(this).width() * 100);
    var mouseYPercent = Math.round(yPos / $(this).height() * 100);

    /* Pos­i­tion Each Layer */
    $(this)
      .find(".parallax")
      .each(function() {
        var diffX = $(".advantages").width() - $(this).width();
        var diffY = $(".advantages").height() - $(this).height();

        var myX = diffX * (mouseXPercent / -2000);
        var myY = diffY * (mouseYPercent / -2000);
        var cssObj = { left: myX + "px", top: myY + "px" };
        $(this).animate(
          { left: myX, top: myY },
          { duration: 50, queue: false }
        );
      });
  });

  if ($(window).width() > 768) {
    $(".main_block .main_block_column.left, .main_block .main_block_column.right").addClass("visible");
    $(window).on("scroll", function() {
      if ($(window).scrollTop() >= $(".our_advantages").offset().top) {
        $(".advantages_item").addClass("visible");
      }
    });
  } else {
    // console.log('sad');
    
    // $(document).on('swipedown', '.our_advantages', function () {
    //   alert("swipedown..");
    // });
    // $(document).on('swipeup', '.swipe', function () {
    //   console.log($(this).next().offset().top)
    //     $("html, body").animate({scrollTop: $(this).next().offset().top}, 500)
    // });
    console.log('swipe');
    
    $(".swipe").swipe( {
      //Generic swipe handler for all directions
      swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
        if (direction === 'up') {
          $("html, body").animate({scrollTop: $(this).next().offset().top}, 500) 
        } 
        if (direction === 'down') {
          $("html, body").animate({scrollTop: $(this).prev().offset().top}, 500)
        }
      }
    });
  }
});

