<?php get_header(); ?>

	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>
			
			<div class="container content_container">
				<h1 class="page_title"><span><?php the_title(); ?></span></h1>
				
				<div class="page_content">
					<?php the_content(); ?>
					<?php if (types_render_field("page_donate_btn", array("output"=>"raw"))) { ?>
						<div class="page_content">
							<div>&nbsp;</div>
							<div style="text-align: center;"><h5 style="text-align: center;">Become our Supporter</h5><br />
							<a href="<?php the_permalink(121); ?>" class="green_btn">Donate Now</a></div>
						</div>
					<?php } ?>
				</div>
			</div>
			
		<?php endwhile; ?>
	<?php else : ?>
		<?php get_template_part( 'content', '404' ); ?>
	<?php endif; // end have_posts() check ?>

<?php get_footer(); ?>